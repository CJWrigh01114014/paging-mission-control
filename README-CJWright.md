# Paging Mission Control, Solution Application by Courtney J. Wright

The solution ingests a text file of pipe-separated telemetry data. The solution program presumes the telemetry data are properly formatted. Per the instructions readme file, there is no error-handling specific to input validation. The solution program expects the input text file to be entered as a command-line argument. If none is provided, the program will execute using an input file provided with the program and outputs a usage statement that precedes the solution output.

# File structure

- paging-mission-control-0.1-SNAPSHOT.jar
- data/telemetry-data

# Execution command

java -jar paging-mission-control-0.1-SNAPSHOT.jar

OR

java -jar paging-mission-control-0.1-SNAPSHOT.jar *file-name*